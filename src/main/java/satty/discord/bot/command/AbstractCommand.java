package satty.discord.bot.command;

import org.springframework.beans.factory.annotation.Autowired;
import satty.discord.bot.DiscordBot;
import satty.discord.bot.entity.DiscordChannel;

public abstract class AbstractCommand implements Command {

    @Autowired
    private DiscordBot discordBot;

    protected void say(DiscordChannel channel, String message, Object... args) {
        discordBot.post(String.format(message, args), channel);
    }
}
