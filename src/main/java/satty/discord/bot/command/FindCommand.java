package satty.discord.bot.command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import satty.discord.bot.DiscordBot;
import satty.discord.bot.entity.DiscordChannel;
import satty.discord.bot.entity.DiscordPost;
import satty.discord.bot.service.SiteService;
import satty.discord.bot.service.YoutubeService;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.List;
import java.util.Optional;

@Component("find")
public class FindCommand extends AbstractCommand {

    @Autowired
    private YoutubeService youtubeService;

    @Autowired
    private SiteService siteService;

    @Autowired
    private DiscordBot discordBot;

    @Override
    public void execute(MessageReceivedEvent event, String... args) {
        String type = args[0];
        Instant fromTime = parseInstant(args[1]);
        Instant toTime = args[2].equals("now") ? Instant.now(Clock.systemDefaultZone()) : parseInstant(args[2]);
        String identity = args[3];
        DiscordChannel discordChannel = new DiscordChannel(event.getAuthor(), event.getChannel());
        if ("youtube".equals(type)) {
            String channelId = Optional.ofNullable(youtubeService.getChannelIdByName(identity)).orElse(identity);
            List<String> videoUrls = youtubeService.getVideosBetween(channelId, fromTime, toTime);
            videoUrls.forEach(discordPost -> discordBot.post(discordPost, discordChannel));
        } else {
            List<DiscordPost> posts = siteService.getPostsBetween(identity, fromTime, toTime);
            posts.forEach(discordPost -> discordBot.post(discordPost, discordChannel));
        }
    }

    private Instant parseInstant(String timestamp) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");

        TemporalAccessor temporalAccessor = formatter.parse(timestamp);
        LocalDateTime localDateTime = LocalDateTime.from(temporalAccessor);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
        return Instant.from(zonedDateTime);
    }
}
