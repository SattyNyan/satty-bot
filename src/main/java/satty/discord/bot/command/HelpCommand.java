package satty.discord.bot.command;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

@Component("help")
public class HelpCommand extends AbstractCommand {

    private static final String HELP_MESSAGE = "Подписаться на ютуб канал: `!subscribe youtube {идентификатор}`\n" +
            "Отписаться от ютуб канала: `!unsubscribe youtube {идентификатор}`\n\n" +
            "В качестве идентификатора можно использовать имя пользователя или айди канала:\n" +
            "**RiotGamesInc** из https://www.youtube.com/user/RiotGamesInc\n" +
            "**UC2t5bjwHdUX4vM2g8TRDq5g** из https://www.youtube.com/channel/UC2t5bjwHdUX4vM2g8TRDq5g";

    @Override
    public void execute(MessageReceivedEvent event, String... args) {
        event.getChannel().sendMessage(new EmbedBuilder().setDescription(HELP_MESSAGE).build()).queue();
    }
}
