package satty.discord.bot.command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;

@Component("ping")
public class PingCommand extends AbstractCommand {

    @Override
    public void execute(MessageReceivedEvent event, String... args) {
        event.getChannel().sendMessage("pong!").queue();
    }
}
