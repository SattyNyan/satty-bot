package satty.discord.bot.command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public interface Command {
    void execute(MessageReceivedEvent event, String... args);
}
