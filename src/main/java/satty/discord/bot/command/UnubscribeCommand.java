package satty.discord.bot.command;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import satty.discord.bot.entity.DiscordChannel;
import satty.discord.bot.entity.Subscription;
import satty.discord.bot.exception.BotServiceException;
import satty.discord.bot.service.SubscriptionService;

@Component("unsubscribe")
public class UnubscribeCommand extends AbstractCommand {

    @Autowired
    private SubscriptionService subscriptionService;

    @Override
    public void execute(MessageReceivedEvent event, String... args) {
        DiscordChannel discordChannel = new DiscordChannel(event.getAuthor(), event.getChannel());
        if (args.length != 2) {
            say(discordChannel, "Wrong arguments count.");
        }
        String subscriptionType = args[0];
        String subscriptionName = args[1];
        try {
            Subscription subscription = subscriptionService.unsubscribe(discordChannel, subscriptionName,
                    subscriptionType);
            say(discordChannel, "Channel [%s] has unsubscribed from [%s].", discordChannel.getName(),
                    subscription.getName());
        } catch (BotServiceException ex) {
            say(discordChannel, ex.getMessage());
        }
    }
}
