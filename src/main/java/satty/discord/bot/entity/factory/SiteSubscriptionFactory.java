package satty.discord.bot.entity.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import satty.discord.bot.entity.SubscriptionType;
import satty.discord.bot.service.SiteService;

@Component("site")
public class SiteSubscriptionFactory extends SubscriptionFactory {

    @Autowired
    private SiteService siteService;

    @Override
    protected String getIdByName(String name) {
        return siteService.getSiteIdByName(name);
    }

    @Override
    protected String getNameById(String id) {
        return siteService.getSiteNameById(id);
    }

    @Override
    protected SubscriptionType getType() {
        return SubscriptionType.SITE;
    }
}
