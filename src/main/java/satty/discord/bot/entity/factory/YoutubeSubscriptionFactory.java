package satty.discord.bot.entity.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import satty.discord.bot.entity.SubscriptionType;
import satty.discord.bot.service.YoutubeService;

@Component("youtube")
public class YoutubeSubscriptionFactory extends SubscriptionFactory {

    @Autowired
    private YoutubeService youtubeService;

    @Override
    protected String getIdByName(String name) {
        return youtubeService.getChannelIdByName(name);
    }

    @Override
    protected String getNameById(String id) {
        return youtubeService.getChannelNameById(id);
    }

    @Override
    protected SubscriptionType getType() {
        return SubscriptionType.YOUTUBE;
    }
}
