package satty.discord.bot.entity.factory;

import satty.discord.bot.entity.Subscription;
import satty.discord.bot.entity.SubscriptionType;
import satty.discord.bot.exception.BotServiceException;

public abstract class SubscriptionFactory {

    public Subscription getSubscription(String identity) {
        String id = getIdByName(identity);
        if (null == id) {
            id = identity;
        }
        String name = getNameById(id);
        if (null == name) {
            throw new BotServiceException(String.format("Can't find subscription with such identity [%s].", identity));
        }
        return new Subscription(id, name, getType());
    }

    protected abstract String getIdByName(String name);

    protected abstract String getNameById(String id);

    protected abstract SubscriptionType getType();
}
