package satty.discord.bot.entity;

import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "DISCORD_SUBSCRIBER")
public class DiscordChannel {
    @Id
    private String id;
    private String authorId;
    private String name;
    @Enumerated(EnumType.STRING)
    private ChannelType type;

    @ManyToMany
    private Set<Subscription> subscriptions;

    public DiscordChannel() {
    }

    public DiscordChannel(User author, MessageChannel messageChannel) {
        this.id = messageChannel.getId();
        this.authorId = author.getId();
        this.type = messageChannel.getType();
        this.name = messageChannel.getName();
    }

    public boolean isPrivate() {
        return type == ChannelType.PRIVATE;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChannelType getType() {
        return type;
    }

    public void setType(ChannelType type) {
        this.type = type;
    }

    public Set<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Set<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public boolean addSubscription(Subscription subscription) {
        if (subscriptions == null) {
            subscriptions = new HashSet<>();
        }
        return this.subscriptions.add(subscription);
    }

    public void removeSubscription(Subscription subscription) {
        subscriptions.remove(subscription);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        DiscordChannel that = (DiscordChannel) object;
        return Objects.equals(id, that.id) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type);
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", name, id);
    }
}
