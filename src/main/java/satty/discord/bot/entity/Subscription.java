package satty.discord.bot.entity;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Subscription {
    @Id
    private String channelId;
    private String name;
    @Enumerated(EnumType.STRING)
    private SubscriptionType type;

    @ManyToMany(mappedBy = "subscriptions")
    private Set<DiscordChannel> subscribers;

    public Subscription() {
    }

    public Subscription(String channelId, String name, SubscriptionType type) {
        this.channelId = channelId;
        this.name = name;
        this.type = type;
    }

    public boolean isSite() {
        return type == SubscriptionType.SITE;
    }

    public boolean hasSubscribers() {
        return !subscribers.isEmpty();
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SubscriptionType getType() {
        return type;
    }

    public void setType(SubscriptionType type) {
        this.type = type;
    }

    public Set<DiscordChannel> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(Set<DiscordChannel> subscribers) {
        this.subscribers = subscribers;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Subscription that = (Subscription) object;
        return this.channelId.equals(that.channelId);
    }

    @Override
    public int hashCode() {
        return channelId.hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", name, channelId);
    }
}
