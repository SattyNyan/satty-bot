package satty.discord.bot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import satty.discord.bot.dao.DiscordChannelRepository;
import satty.discord.bot.dao.SubscriptionRepository;
import satty.discord.bot.entity.DiscordChannel;
import satty.discord.bot.entity.Subscription;
import satty.discord.bot.entity.factory.SubscriptionFactory;
import satty.discord.bot.exception.BotServiceException;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class SubscriptionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionService.class);

    @Autowired
    private DiscordChannelRepository discordChannelRepository;
    @Autowired
    private SubscriptionRepository subscriptionRepository;
    @Autowired
    private Map<String, SubscriptionFactory> subscriptionFactoryMap;

    public Subscription subscribe(DiscordChannel discordChannel, String subscriptionName, String subscriptionType) {
        DiscordChannel subscriber = discordChannelRepository.findById(discordChannel.getId()).orElse(discordChannel);
        Subscription subscription = getSubscription(subscriptionName, subscriptionType);
        if (!subscriber.addSubscription(subscription)) {
            String message = String.format("Subscriber [%s] already subscribed on [%s].", subscriber, subscription);
            LOGGER.info(message);
            throw new BotServiceException(message);
        }
        subscriptionRepository.save(subscription);
        discordChannelRepository.save(subscriber);
        LOGGER.info("Subscriber [{}] has subscribed on [{}]", subscriber, subscription);
        return subscription;
    }

    public Subscription unsubscribe(DiscordChannel discordChannel, String subscriptionName, String subscriptionType) {
        DiscordChannel subscriber = discordChannelRepository.findById(discordChannel.getId())
                .orElseThrow(() -> new BotServiceException("This channel has no subscriptions."));
        Subscription subscription = getSubscription(subscriptionName, subscriptionType);
        subscriber.removeSubscription(subscription);
        subscriptionRepository.save(subscription);
        discordChannelRepository.save(subscriber);
        LOGGER.info("Subscriber [{}] has unsubscribed from [{}]", subscriber, subscription);
        return subscription;
    }

    public List<Subscription> findAllSubscriptions() {
        return subscriptionRepository.findAll().stream()
                .filter(Subscription::hasSubscribers)
                .collect(Collectors.toList());
    }

    private Subscription getSubscription(String name, String type) {
        return subscriptionFactoryMap.get(type).getSubscription(name);
    }
}
