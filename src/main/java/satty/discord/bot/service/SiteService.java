package satty.discord.bot.service;

import org.springframework.stereotype.Component;
import satty.discord.bot.entity.DiscordPost;
import satty.discord.bot.parser.Site;
import satty.discord.bot.parser.SiteParser;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

@Component
public class SiteService {

    @Resource(name = "siteParsersMap")
    private Map<String, SiteParser> parsers;

    public String getSiteIdByName(String name) {
        return Arrays.stream(Site.values())
                .filter(site -> site.getSiteName().equals(name))
                .findAny()
                .map(Site::getSiteId).orElse(null);
    }

    public String getSiteNameById(String id) {
        return Arrays.stream(Site.values())
                .filter(site -> site.getSiteId().equals(id))
                .findAny()
                .map(Site::getSiteName).orElse(null);
    }

    public List<DiscordPost> getPostsBetween(String siteName, Instant from, Instant to) {
        return parsers.get(siteName).getPostsBetween(from, to);
    }
}
