package satty.discord.bot.service;

import com.google.api.client.util.DateTime;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class YoutubeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(YoutubeService.class);

    private static final String YOUTUBE_URL_FORMAT = "https://www.youtube.com/watch?v=%s";
    private static final long YOUTUBE_MAX_RESULTS = 30L;

    @Autowired
    private YouTube youTube;

    public String getChannelIdByName(String name) {
        try {
            List<Channel> channels = youTube.channels().list("id").setForUsername(name).execute().getItems();
            return !channels.isEmpty() ? channels.get(0).getId() : null;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getChannelNameById(String id) {
        try {
            List<Channel> channels = youTube.channels().list("snippet").setId(id).execute().getItems();
            return !channels.isEmpty() ? channels.get(0).getSnippet().getTitle() : null;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<String> getVideosBetween(String channelId, Instant from, Instant to) {
        try {
            DateTime googleFrom = new DateTime(Date.from(from));
            DateTime googleTo = new DateTime(Date.from(to.minusNanos(1)));
            LOGGER.info("\t\tExecuting search request from [{}] to [{}]", googleFrom, googleTo);
            SearchListResponse searchResponse = youTube.search().list("snippet")
                    .setChannelId(channelId)
                    .setOrder("date")
                    .setMaxResults(YOUTUBE_MAX_RESULTS)
                    .setPublishedAfter(googleFrom)
                    .setPublishedBefore(googleTo)
                    .execute();
            List<String> videoLinks = searchResponse.getItems().stream()
                    .map(SearchResult::getId)
                    .map(ResourceId::getVideoId)
                    .map(this::formVideoLink)
                    .collect(Collectors.toList());
            Collections.reverse(videoLinks);
            LOGGER.info("\t\tLinks to send: [{}]", videoLinks);
            return videoLinks;
        } catch (IOException e) {
            LOGGER.error("\t\tException occured: [{}].", e.getMessage(), e);
            return new ArrayList<>();
        }
    }

    private String formVideoLink(String videoId) {
        return String.format(YOUTUBE_URL_FORMAT, videoId);
    }
}
