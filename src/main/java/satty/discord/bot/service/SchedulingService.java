package satty.discord.bot.service;

import org.springframework.stereotype.Component;
import satty.discord.bot.task.BotTask;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class SchedulingService {

    private ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    public void schedule(BotTask task) {
        scheduler.scheduleAtFixedRate(task, 0, task.getRate(), task.getTimeUnit());
    }
}
