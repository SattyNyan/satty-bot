package satty.discord.bot.listener;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import satty.discord.bot.command.Command;

import java.util.Arrays;
import java.util.Map;

@Component
public class CommandListener extends ListenerAdapter {

    @Autowired
    private Map<String, Command> commands;

    @Value("${discord.bot.command.prefix:!}")
    private String commandPrefix;

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getAuthor().isBot()) {
            return;
        }
        String message = event.getMessage().getContentRaw();
        if (message.startsWith(commandPrefix)) {
            CommandBody commandBody = new CommandBody(message);
            if (commands.containsKey(commandBody.command)) {
                commands.get(commandBody.command).execute(event, commandBody.args);
            }
        }
    }

    private class CommandBody {
        private String command;
        private String[] args;

        public CommandBody(String message) {
            String[] commandParts = message.substring(commandPrefix.length()).split(" ");
            command = commandParts[0];
            args = Arrays.copyOfRange(commandParts, 1, commandParts.length);
        }
    }
}
