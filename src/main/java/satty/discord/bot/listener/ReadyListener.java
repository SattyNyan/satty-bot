package satty.discord.bot.listener;

import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import satty.discord.bot.service.SchedulingService;
import satty.discord.bot.task.BotTask;

import java.util.List;

@Component
public class ReadyListener extends ListenerAdapter {

    @Autowired
    private List<BotTask> tasks;

    @Autowired
    private SchedulingService schedulingService;

    @Override
    public void onReady(ReadyEvent event) {
        tasks.forEach(schedulingService::schedule);
    }
}
