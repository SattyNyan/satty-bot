package satty.discord.bot.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import satty.discord.bot.parser.SiteParser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class SiteParsersConfiguration {

    @Autowired
    private List<SiteParser> parsers;

    @Bean("siteParsersMap")
    public Map<String, SiteParser> siteParsersMap() {
        Map<String, SiteParser> map = new HashMap<>();
        parsers.forEach(siteParser -> map.put(siteParser.getSite().getSiteName(), siteParser));
        return map;
    }
}
