package satty.discord.bot.configuration;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeRequestInitializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class YoutubeConfiguration {

    @Value("${youtube.apikey}")
    private String apiKey;

    @Bean
    public YouTube provideYoutube() {
        return new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), null)
                .setYouTubeRequestInitializer(new YouTubeRequestInitializer(apiKey))
                .build();
    }
}
