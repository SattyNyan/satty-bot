package satty.discord.bot.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import satty.discord.bot.task.ScanTask;

import java.util.concurrent.TimeUnit;

@Configuration
public class TasksConfiguration {

    @Value("${discord.bot.scan.rate}")
    private long scanRate;

    @Bean
    public ScanTask scanTask() {
        return new ScanTask(scanRate, TimeUnit.MINUTES);
    }
}
