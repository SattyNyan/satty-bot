package satty.discord.bot;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import satty.discord.bot.entity.DiscordChannel;
import satty.discord.bot.entity.DiscordPost;

import javax.annotation.PostConstruct;
import javax.security.auth.login.LoginException;

@Component
public class DiscordBot {

    private static final Logger LOGGER = LoggerFactory.getLogger(DiscordBot.class);

    @Value("${discord.bot.token}")
    private String botToken;
    @Autowired
    private ListenerAdapter[] listenerAdapters;

    private JDA jda;

    public JDA getJda() {
        return jda;
    }

    @PostConstruct
    public void login() throws LoginException {
        jda = new JDABuilder(AccountType.BOT)
                .setToken(botToken)
                .addEventListener(listenerAdapters)
                .build();
    }

    public void post(String message, DiscordChannel subscriber) {
        LOGGER.info("\tSending message [{}] into discord channel [{}]", message, subscriber);
        MessageChannel channel = selectChannel(subscriber);
        channel.sendMessage(message).queue();
    }

    public void post(DiscordPost post, DiscordChannel discordChannel) {
        LOGGER.info("\tSending post [{}] into discord channel [{}]", post, discordChannel);
        MessageChannel channel = selectChannel(discordChannel);
        channel.sendMessage(buildEmbed(post)).queue();
    }

    private MessageChannel selectChannel(DiscordChannel subscriber) {
        return subscriber.isPrivate() ? getPrivateChannel(subscriber)
                : jda.getTextChannelById(subscriber.getId());
    }

    private MessageChannel getPrivateChannel(DiscordChannel subscriber) {
        MessageChannel channel = jda.getPrivateChannelById(subscriber.getId());
        return null == channel ? jda.getUserById(subscriber.getAuthorId()).openPrivateChannel().complete() : channel;
    }

    private MessageEmbed buildEmbed(DiscordPost post) {
        return new EmbedBuilder()
                .setTitle(post.getTitle(), post.getPostUrl())
                .setImage(post.getPostImage())
                .setDescription(post.getContent())
                .setAuthor(post.getSiteName(), post.getSiteUrl(), post.getSiteIcon())
                .setColor(post.getColor())
                .build();
    }
}
