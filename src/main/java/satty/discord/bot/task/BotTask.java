package satty.discord.bot.task;

import java.util.concurrent.TimeUnit;

public abstract class BotTask implements Runnable {

    private long rate;
    private TimeUnit timeUnit;

    public BotTask(long rate) {
        this(rate, TimeUnit.MINUTES);
    }

    public BotTask(long rate, TimeUnit timeUnit) {
        this.rate = rate;
        this.timeUnit = timeUnit;
    }

    public long getRate() {
        return rate;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }
}
