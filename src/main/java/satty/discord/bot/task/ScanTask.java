package satty.discord.bot.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import satty.discord.bot.DiscordBot;
import satty.discord.bot.entity.DiscordChannel;
import satty.discord.bot.entity.DiscordPost;
import satty.discord.bot.entity.Subscription;
import satty.discord.bot.service.SiteService;
import satty.discord.bot.service.SubscriptionService;
import satty.discord.bot.service.YoutubeService;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ScanTask extends BotTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScanTask.class);

    private static final long SCAN_OFFSET = 1;

    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private YoutubeService youtubeService;
    @Autowired
    private SiteService siteService;
    @Autowired
    private DiscordBot discordBot;

    public ScanTask(long rate) {
        super(rate);
    }

    public ScanTask(long rate, TimeUnit timeUnit) {
        super(rate, timeUnit);
    }

    @Override
    public void run() {
        try {
            Instant scanTime = Instant.now();
            Instant toTime = scanTime.truncatedTo(ChronoUnit.MINUTES).minus(SCAN_OFFSET, ChronoUnit.MINUTES);
            Instant fromTime = toTime.minus(this.getRate(), ChronoUnit.MINUTES);
            LOGGER.info("");
            LOGGER.info("Starting [{}]", this.getClass().getName());
            List<Subscription> subscriptions = subscriptionService.findAllSubscriptions();
            LOGGER.info("\tGot [{}] subscriptions.", subscriptions.size());
            for (Subscription subscriprion : subscriptions) {
                LOGGER.info("\tScanning [{}] for channels [{}]", subscriprion, subscriprion.getSubscribers());
                if (subscriprion.isSite()) {
                    List<DiscordPost> posts = siteService.getPostsBetween(subscriprion.getName(), fromTime, toTime);
                    for (DiscordChannel discordChannel : subscriprion.getSubscribers()) {
                        posts.forEach(discordPost -> discordBot.post(discordPost, discordChannel));
                    }
                } else {
                    List<String> videoUrls =
                            youtubeService.getVideosBetween(subscriprion.getChannelId(), fromTime, toTime);
                    for (DiscordChannel discordChannel : subscriprion.getSubscribers()) {
                        videoUrls.forEach(discordPost -> discordBot.post(discordPost, discordChannel));
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception occured: [{}].", e.getMessage(), e);
        }
    }
}
