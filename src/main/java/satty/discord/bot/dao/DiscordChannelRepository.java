package satty.discord.bot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import satty.discord.bot.entity.DiscordChannel;

@Repository
public interface DiscordChannelRepository extends JpaRepository<DiscordChannel, String> {
}
