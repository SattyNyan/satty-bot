package satty.discord.bot.exception;

import satty.discord.bot.entity.DiscordChannel;

public class BotCommandException extends RuntimeException {

    private DiscordChannel discordChannel;

    public BotCommandException(DiscordChannel discordChannel) {
        this.discordChannel = discordChannel;
    }

    public BotCommandException(String message, DiscordChannel discordChannel) {
        super(message);
        this.discordChannel = discordChannel;
    }

    public BotCommandException(String message, Throwable cause, DiscordChannel discordChannel) {
        super(message, cause);
        this.discordChannel = discordChannel;
    }

    public DiscordChannel getDiscordChannel() {
        return discordChannel;
    }
}
