package satty.discord.bot.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import satty.discord.bot.entity.DiscordPost;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

public abstract class SiteParser {

    public abstract List<DiscordPost> getPostsAfterDate(Instant date);

    public abstract List<DiscordPost> getPostsBetween(Instant from, Instant to);

    public abstract Site getSite();

    protected DiscordPost buildDiscordPost(String postUrl) {
        DiscordPost discordPost = new DiscordPost();
        Site site = getSite();
        discordPost.setSiteName(site.getSiteName());
        discordPost.setSiteUrl(site.getSiteUrl());
        discordPost.setSiteIcon(site.getSiteIcon());
        discordPost.setColor(site.getSiteColor());
        discordPost.setPostUrl(postUrl);

        Element htmlPost = connect(postUrl);
        discordPost.setTitle(parseTitle(htmlPost));
        discordPost.setPostImage(parseImage(htmlPost));
        discordPost.setContent(parseContent(htmlPost));
        return discordPost;
    }

    protected Document connect(String url) {
        try {
            return Jsoup.connect(url).get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected abstract String parseTitle(Element htmlPost);

    protected abstract String parseContent(Element htmlPost);

    protected abstract String parseImage(Element htmlPost);
}
