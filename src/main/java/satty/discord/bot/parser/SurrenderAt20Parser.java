package satty.discord.bot.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import satty.discord.bot.entity.DiscordPost;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Component
public class SurrenderAt20Parser extends SiteParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(SurrenderAt20Parser.class);

    private static final String SEARCH_URL_PATTERN =
            "https://www.surrenderat20.net/search?updated-min=%s&updated-max=%s&max-results=30";

    private static final String IMG_TAG = "img";
    private static final String IMG_SRC_ATTR = "src";
    private static final String A_TAG = "a";
    private static final String A_HREF_ATTR = "href";

    private static final String TITLE_CLASS_NAME = "news-title";
    private static final String CONTENT_CLASS_NAME = "news-content";
    private static final String TABLE_OF_CONTENTS_ID_NAME = "toc";

    private static final String CONTENT_TITLE = "**Table of contents**";
    private static final String CONTENT_LINK_FORMAT = "[%s](%s)";
    private static final String CONTENT_FOOTER = "and more...";
    private static final String CONTENT_DELIMITER = "\n";
    private static final int MAX_CONTENT_LENGTH = 1900;

    @Override
    public List<DiscordPost> getPostsAfterDate(Instant date) {
        LOGGER.info("\t\tExecuting search request from [{}]", date.atZone(ZoneId.systemDefault()));
        String formattedDate = date.atZone(ZoneId.of("-8")).withNano(0).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        String searchUrl = String.format(SEARCH_URL_PATTERN, formattedDate);
        Document site = connect(searchUrl);
        List<String> postLinks = site.getElementsByClass(TITLE_CLASS_NAME).stream()
                .map(elem -> getAttribute(elem, A_TAG, A_HREF_ATTR))
                .collect(Collectors.toList());
        LOGGER.info("\t\tPost links found [{}]", postLinks);
        return postLinks.stream()
                .map(this::buildDiscordPost)
                .collect(Collectors.toList());
    }

    @Override
    public List<DiscordPost> getPostsBetween(Instant from, Instant to) {
        LOGGER.info("\t\tExecuting search request from [{}] to [{}]", from.atZone(ZoneId.systemDefault()),
                to.atZone(ZoneId.systemDefault()));
        String searchUrl = String.format(SEARCH_URL_PATTERN, formatDate(from), formatDate(to));
        Document site = connect(searchUrl);
        List<String> postLinks = site.getElementsByClass(TITLE_CLASS_NAME).stream()
                .map(elem -> getAttribute(elem, A_TAG, A_HREF_ATTR))
                .collect(Collectors.toList());
        Collections.reverse(postLinks);
        LOGGER.info("\t\tPost links found [{}]", postLinks);
        return postLinks.stream()
                .map(this::buildDiscordPost)
                .collect(Collectors.toList());
    }

    @Override
    public Site getSite() {
        return Site.SURRENDER_AT_20;
    }

    @Override
    protected String parseTitle(Element htmlPost) {
        String rawTitle = htmlPost.getElementsByClass(TITLE_CLASS_NAME).html();
        return unescape(rawTitle);
    }

    @Override
    protected String parseImage(Element htmlPost) {
        return htmlPost.getElementsByClass(CONTENT_CLASS_NAME).stream()
                .map(element -> getAttribute(element, IMG_TAG, IMG_SRC_ATTR))
                .filter(Objects::nonNull)
                .findFirst().orElse(null);
    }

    @Override
    protected String parseContent(Element htmlPost) {
        Element tocElement = htmlPost.getElementById(TABLE_OF_CONTENTS_ID_NAME);
        if (null != tocElement) {
            Element contentList = Optional.of(tocElement)
                    .map(Element::nextElementSibling)
                    .orElse(tocElement.parent().nextElementSibling());

            String contentBody = contentList.children().stream()
                    .filter(Element::hasText)
                    .map(Element::children)
                    .flatMap(Collection::stream)
                    .map(this::buildContentUrl)
                    .collect(Collectors.joining("\n"));
            return buildContent(contentBody);
        }
        return null;
    }

    private String unescape(String text) {
        return Jsoup.parse(text).text();
    }

    private String buildContentUrl(Element contentElement) {
        String title = unescape(contentElement.html());
        String href = getAttribute(contentElement, A_TAG, A_HREF_ATTR);
        return String.format(CONTENT_LINK_FORMAT, title, href);
    }

    private String getAttribute(Element from, String tag, String attrKey) {
        String attr = from.getElementsByTag(tag).attr(attrKey);
        return attr.isEmpty() ? null : attr;
    }

    private String buildContent(String contentBody) {
        String croppedContentBody = crop(contentBody);
        StringJoiner contentJoiner = new StringJoiner(CONTENT_DELIMITER)
                .add(CONTENT_TITLE)
                .add(croppedContentBody);
        return contentBody.equals(croppedContentBody) ? contentJoiner.toString()
                : contentJoiner.add(CONTENT_FOOTER).toString();
    }

    private String crop(String string) {
        int lastDelimiter = string.lastIndexOf(CONTENT_DELIMITER);
        if (lastDelimiter < 0) {
            return string;
        }
        String newString = string.substring(0, lastDelimiter);
        return string.length() > MAX_CONTENT_LENGTH ? crop(newString) : string;
    }

    private String formatDate(Instant date) {
        return date.atZone(ZoneId.of("-8")).withNano(0).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }
}
