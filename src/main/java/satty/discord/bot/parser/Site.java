package satty.discord.bot.parser;

import java.awt.Color;

public enum Site {

    SURRENDER_AT_20("surrenderat20id", "Surrender@20", "https://www.surrenderat20.net",
            "https://3.bp.blogspot.com/-M_ecJWWc5CE/Uizpk6U3lwI/AAAAAAAACLo/xyh6eQNRzzs/s640/sitethumb.jpg",
            new Color(191, 52, 30));

    private String siteId;
    private String siteName;
    private String siteUrl;
    private String siteIcon;
    private Color siteColor;

    Site(String siteId, String siteName, String siteUrl, String siteIcon, Color siteColor) {
        this.siteId = siteId;
        this.siteName = siteName;
        this.siteUrl = siteUrl;
        this.siteIcon = siteIcon;
        this.siteColor = siteColor;
    }

    public String getSiteId() {
        return siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public String getSiteIcon() {
        return siteIcon;
    }

    public Color getSiteColor() {
        return siteColor;
    }
}
